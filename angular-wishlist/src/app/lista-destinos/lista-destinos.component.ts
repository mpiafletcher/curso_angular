import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { DestinoViaje } from '../models/destino-viaje.model';
import { DestinoApiClient } from '../models/destino-api-client.model';
import { TitleCasePipe } from '@angular/common';

@Component({
  selector: 'app-lista-destinos',
  templateUrl: './lista-destinos.component.html',
  styleUrls: ['./lista-destinos.component.css'],
})
export class ListaDestinosComponent implements OnInit {
  @Output() onItemAdded: EventEmitter<DestinoViaje>;
  updates: string[];

  constructor(private destinoApiClient: DestinoApiClient) {
    this.onItemAdded = new EventEmitter();
    this.updates = [];
    this.destinoApiClient.subscribeOnChange((d: DestinoViaje) => {
      if(d != null) {
        this.updates.push("se ha elegido a" + d.nombre);
      }
    });
  }

  ngOnInit() {

  }

 /* guardar(nombre: string, url: string): boolean {
    this.destinos.push(new DestinoViaje(nombre, url));
  //  this.destinoApiClient.add(d);
    return false;
  }*/

  agregado(d: DestinoViaje) {
    this.destinoApiClient.add(d);
    this.onItemAdded.emit(d);
  }

  elegido(e: DestinoViaje) {
      this.destinoApiClient.elegir(e);
    }
}
